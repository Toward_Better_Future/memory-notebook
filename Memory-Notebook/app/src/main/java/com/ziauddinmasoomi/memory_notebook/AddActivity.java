package com.ziauddinmasoomi.memory_notebook;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AddActivity extends AppCompatActivity implements View.OnClickListener {
    private Button add,reset;
    private EditText titleF,timeF,dateF,locationF,descriptionF;
    private TextView imageAddress;
    private MemoryList memoryList;
    private MainActivity mainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        add = (Button) findViewById(R.id.saveBtnA);
        reset = (Button) findViewById(R.id.resetBtnA);
        titleF = (EditText) findViewById(R.id.titleA);
        timeF = (EditText) findViewById(R.id.timeA);
        dateF = (EditText) findViewById(R.id.dateA);
        locationF = (EditText) findViewById(R.id.locationA);
        descriptionF = (EditText) findViewById(R.id.descriptionA);
    }

    @Override
    protected void onStart() {
        super.onStart();
        add.setOnClickListener(this);
        reset.setOnClickListener(this);
    }

    public void onClick(View v){


        if(v.getId() == R.id.saveBtnA){
            MemoryList.memories.add(new Memory(1,titleF.getText().toString().trim(),timeF.getText().toString().trim(),
                    dateF.getText().toString().trim(),locationF.getText().toString().trim(),descriptionF.getText().toString().trim()));
            Toast.makeText(AddActivity.this,"Memory saved successfully.",Toast.LENGTH_SHORT).show();
            reset();
        }
        else if (v.getId() == R.id.resetBtnA){
           reset();
        }
    }

    private void reset() {
        titleF.setText("");
        timeF.setText("");
        dateF.setText("");
        locationF.setText("");
        descriptionF.setText("");
    }
}
