package com.ziauddinmasoomi.memory_notebook;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class InfoActivity extends AppCompatActivity {
    private TextView titleF,timeF,dateF,locationF,descriptionF;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        titleF = (TextView) findViewById(R.id.titleI);
        timeF = (TextView) findViewById(R.id.timeI);
        dateF = (TextView) findViewById(R.id.dateI);
        locationF = (TextView) findViewById(R.id.locationI);
        descriptionF = (TextView) findViewById(R.id.descriptionI);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Bundle extras = getIntent().getExtras();
        titleF.setText(" Title : "+extras.getString("title"));
        timeF.setText(" Tiem : "+extras.getString("time"));
        dateF.setText(" Date : "+extras.getString("date"));
        locationF.setText(" Location : "+extras.getString("location"));
        descriptionF.setText(" Description : "+extras.getString("description"));
    }
}
