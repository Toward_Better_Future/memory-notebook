package com.ziauddinmasoomi.memory_notebook;

/**
 * Created by Zia Uddin Masoomi on 4/18/2017.
 */

public class Memory {
    private static  int id;
    private int imageAddress;
    private String title;
    private String time;
    private String date;
    private String location;
    private String description;

    private Memory(){}

    public Memory(int imageAddress, String title, String time, String date, String location, String description) {
        id++;
        this.imageAddress = imageAddress;
        this.title = title;
        this.time = time;
        this.date = date;
        this.location = location;
        this.description = description;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        Memory.id = id;
    }

    public int getImageAddress() {
        return imageAddress;
    }

    public void setImageAddress(int imageAddress) {
        this.imageAddress = imageAddress;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return getTitle();
    }
}
