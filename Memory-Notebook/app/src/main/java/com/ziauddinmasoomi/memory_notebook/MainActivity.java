package com.ziauddinmasoomi.memory_notebook;

import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Memory> arrayList;
    private ArrayAdapter<Memory> adapter;
    public ListView listView;
    private MenuItem item;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listArrayAdapter();
    }

    private void listArrayAdapter() {
//        arrayList = new ArrayList<>();
//        arrayList.add(new Memory(1,"Book","3:30","10/15/2013","Herat","It was fantastic"));
//        arrayList.add(new Memory(2,"Notebook","4:30","10/15/2014","Kabul","It was awson"));
//        arrayList.add(new Memory(3,"Pencil","6:30","10/15/2016","Farooq","It was bad"));
//        arrayList.add(new Memory(4,"Pen","8:30","10/15/2012","Qandhar","It was good"));
//        arrayList.add(new Memory(5,"Man","2:30","10/15/2011","Badqis","It was ver excyty"));
//        arrayList.add(new Memory(6,"Woman","2:30","10/15/2012","Mazar","It was fantastic"));
//        arrayList.add(new Memory(7,"Baby","0:30","10/15/2014","Zabol","It was nice"));
//        arrayList.add(new Memory(8,"Cow","12:30","10/15/2014","Jumal","It was very bad"));
//        arrayList.add(new Memory(9,"Bicycle","11:30","10/15/2015","Qarchaqay","It was not bad"));
//        arrayList.add(new Memory(10,"Desk","4:30","10/15/2019","Zahedan","It was excellent"));
//        arrayList.add(new Memory(11,"Computer","3:30","10/15/2013","Iran","It was good"));
//
//        adapter = new ArrayAdapter<Memory>(MainActivity.this,R.layout.support_simple_spinner_dropdown_item,arrayList);
        listView = (ListView) findViewById(R.id.memoryList);
//        listView.setAdapter(adapter);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                return false;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Memory memory =  (Memory)parent.getAdapter().getItem(position);
                Intent intent = new Intent(MainActivity.this,InfoActivity.class);
                intent.putExtra("title",memory.getTitle().toString());
                intent.putExtra("time",memory.getTime().toString());
                intent.putExtra("date",memory.getDate().toString());
                intent.putExtra("location",memory.getLocation().toString());
                intent.putExtra("description",memory.getDescription().toString());
                intent.putExtra("imageId",memory.getImageAddress());
                startActivity(intent);
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menubar,menu);
        item = menu.findItem(R.id.search_field);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.newMemoryBtn:
                startActivity(new Intent(MainActivity.this,AddActivity.class));
                return true;
            case R.id.about:
                Toast.makeText(MainActivity.this,"This application is made by Zia Uddin Masoomi.",Toast.LENGTH_LONG).show();
               return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
