package com.ziauddinmasoomi.memory_notebook;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

/**
 * Created by Zia Uddin Masoomi on 4/18/2017.
 */

public class Helper {

    private MemoryList memoryList;
    private ArrayList<Memory> arrayList;
    private ArrayAdapter<Memory> adapter;
    private Context mContext;
    public Helper(){
        memoryList = new MemoryList();
        arrayList = memoryList.memories;

    }

    public void arrayAdapter(MainActivity activity){

        adapter = new ArrayAdapter<Memory>(mContext,R.layout.support_simple_spinner_dropdown_item,arrayList);
        activity.listView.setAdapter(adapter);
    }
}
